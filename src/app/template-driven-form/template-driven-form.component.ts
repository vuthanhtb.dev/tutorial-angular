import { Component } from '@angular/core';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.scss']
})
export class TemplateDrivenFormComponent {
  firstName: string = '';
  lastName: string = '';

  constructor(private commonService: CommonService) {}

  submitForm(): void {
    this.commonService.submitData(this.firstName, this.lastName);
  }
}
