import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.scss']
})
export class QrcodeComponent {
  qrInfo: string = 'https://www.npmjs.com/package/angular2-qrcode';
  baseUrl: string = 'https://www.npmjs.com/package/angular2-qrcode';
  name: string = '';
  age: string = '';

  constructor(private route: ActivatedRoute) {
    route.queryParams.subscribe((query) => {
      console.log(query);
      if (query && query['data']) {
        const data = query['data'];
        console.log(JSON.parse(data));
      }
    });
  }

  onChangeBaseUrl(event: any): void {
    this.baseUrl = event.target.value;
    this.combine();
  }

  onChangeName(event: any): void {
    this.name = event.target.value;
    this.combine();
  }

  onChangeAge(event: any): void {
    this.age = event.target.value;
    this.combine();
  }

  combine(): void {
    const data = JSON.stringify({name: this.name, age: this.age});
    this.qrInfo = `${this.baseUrl}?data=${data}`;
  }
}
