import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) { }

  logout(): void {
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }

  login(username: string): void {
    if (username) {
      this.loggedIn.next(true);
      this.router.navigate(['/home']);
    }
  }

  isLoggedIn(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }
}
