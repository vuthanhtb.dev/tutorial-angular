import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private counter: number = 10;

  constructor() { }

  sqrt(n: number): number {
    return n * n;
  }

  getCounter(): number {
    return this.counter;
  }

  setCounter(n: number): void {
    this.counter = n;
  }

  submitData(firstName: string, lastName: string): void {
    console.log({firstName, lastName});
  }
}
