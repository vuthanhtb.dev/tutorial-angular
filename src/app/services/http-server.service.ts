import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentDto, CommentModel } from '../comments/comment';

@Injectable({
  providedIn: 'root'
})
export class HttpServerService {
  private REST_API_SERVER: string = 'http://localhost:3000';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
  };

  constructor(private httpClient: HttpClient) { }

  getComments(): Observable<Array<CommentModel>> {
    const url = `${this.REST_API_SERVER}/comments`;
    return this.httpClient.get<Array<CommentModel>>(url, this.httpOptions);
  }

  postComment(commentDto: CommentDto): Observable<CommentDto> {
    const url = `${this.REST_API_SERVER}/comments`;
    return this.httpClient.post<CommentDto>(url, commentDto, this.httpOptions);
  }
}
