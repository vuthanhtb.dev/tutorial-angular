import { Component, ViewChild } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent {

  chatMess: string = '';

  @ViewChild('child1') child1: ChildComponent | undefined;
  @ViewChild('child2') child2: ChildComponent | undefined;

  callParent(name: string): void {
    this.chatMess = `Hello ${name}`;
  }

  shareAmount(): void {
    this.child1?.shareAmount(10);
    this.child2?.shareAmount(20);
  }

}
