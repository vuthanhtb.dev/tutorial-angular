import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public name: String = 'Angular';
  public age: Number = 15;
  public fruits: Array<String> = ['Ổi', 'Táo', 'Nho', 'Xoài'];
  public fruitsAndPrice = [
    {name: 'Ổi', price: 30000, saleOff: true},
    {name: 'Táo', price: 12000, saleOff: false},
    {name: 'Nho', price: 32000, saleOff: true},
    {name: 'Xoài', price: 1500, saleOff: false},
    {name: 'TEST', price: -1500, saleOff: false},
  ];

  public provinces = [
    {
      city: 'Chọn Tỉnh/ Thành phố',
      district: ['Chọn Quận/ Huyện']
    },
    {
      city: 'Thái Bình',
      district: ['Tiền Hải', 'Hưng Hà', 'Kiến Xương', 'Đông Hưng', 'Quỳnh Phụ', 'Thái Thụy', 'Vũ Thư']
    },
    {
      city: 'Hà Nội',
      district: ['Ba Đình', 'Bắc Từ Liêm', 'Cầu Giấy', 'Đống Đa', 'Hà Đông', 'Hai Bà Trưng', 'Hoàn Kiếm', 'Hoàng Mai', 'Long Biên', 'Nam Từ Liêm', 'Tây Hồ', 'Thanh Xuân']
    }
  ];
  public districts: Array<String> = ['Chọn Quận/ Huyện'];

  public counter: number = 0;
  public counterSqrt: number = 0;

  constructor(private commonService: CommonService) {
  }

  ngOnInit(): void {
    this.counter = this.commonService.getCounter();
    this.counterSqrt = this.commonService.sqrt(this.counter);
    this.commonService.setCounter(this.counter + 1);
  }

  onChangeProvince(event: any): void {
    this.districts = this.provinces.find((province) => province.city === event.target.value)?.district || [];
  }

}
