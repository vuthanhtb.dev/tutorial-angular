import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  loginName: String = 'user';
  public counter: number = 0;
  public counterSqrt: number = 0;

  constructor(private commonService: CommonService) {
  }
  ngOnInit(): void {
    this.counter = this.commonService.getCounter();
    this.counterSqrt = this.commonService.sqrt(this.counter);
    this.commonService.setCounter(this.counter + 1);
  }
}
