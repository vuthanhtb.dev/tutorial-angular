import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  public formData: FormGroup;
  public formDataBuild: FormGroup;

  constructor(private commonService: CommonService, private formBuilder: FormBuilder) {
    this.formData = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
    });
    this.formDataBuild = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: [''],
    });
  }

  ngOnInit(): void {

  }

  onSubmit(): void {
    console.log(this.formDataBuild.valid)
    const { firstName, lastName } = this.formDataBuild.value;
    this.commonService.submitData(firstName, lastName);
  }
}
