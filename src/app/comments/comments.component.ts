import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpServerService } from '../services/http-server.service';
import { CommentModel } from './comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  public comments: Array<CommentModel> = [];
  public formDataBuild: FormGroup;

  constructor(private formBuilder: FormBuilder, private httpServer: HttpServerService) {
    this.formDataBuild = this.formBuilder.group({
      postId: [1],
      body: [''],
    });
  }

  ngOnInit(): void {
    this.getComments();
  }

  private getComments() {
    this.httpServer.getComments().subscribe((data) => {
      this.comments = data;
    });
  }

  onSubmit(): void {
    console.log(this.formDataBuild.valid)
    const { postId, body } = this.formDataBuild.value;
    this.httpServer.postComment({postId, body}).subscribe((data) => {
      console.log(data);
      this.getComments();
    });
  }
}
