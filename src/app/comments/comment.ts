export interface CommentModel {
  id: number,
  postId: number,
  body: string
}

export interface CommentDto {
  postId: number,
  body: string
}
