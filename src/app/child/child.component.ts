import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent {
  @Input() name: string = '';
  @Output() childCall: EventEmitter<any> = new EventEmitter();

  callParent(): void {
    this.childCall.emit(this.name);
  }
  
  shareAmount(amount: number): void {
    console.log(`child is ${this.name} & ${amount} VND`);
  }
}
