import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit {
  @Input() appHighlight = 'white';

  constructor(private el: ElementRef) { 
    console.log('HighlightDirective');
  }

  ngOnInit(): void {
    this.el.nativeElement.style.color = this.appHighlight;
  }

}
